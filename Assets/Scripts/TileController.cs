﻿using UnityEngine;
using System.Collections;

public class TileController : MonoBehaviour {

    private int boardWidth = 8;
    private int boardHeight = 3;
    public Sprite tile_main;


    void Start () {
        createTiles(0);
        createTiles(1);
    }

    void Update () {
    }



    void createTiles (int player) {
        for (int x = 0; x < boardWidth; x++) {
            for (int y = 0; y < boardHeight; y++) {
                GameObject tile_go = new GameObject();
                //set tile Sprite renderer
                SpriteRenderer tile_sr = tile_go.AddComponent<SpriteRenderer>();
                tile_sr.sprite = tile_main;
                //set position
                if (player == 0) {
                    tile_go.transform.position = new Vector3(x, y - 3, 0);
                    tile_go.name = "Tile(P1" + "," + x + "," + y + ")";
                } else if (player == 1) {
                    tile_go.transform.position = new Vector3(x, y + 2, 0);
                    tile_go.name = "Tile(P2" + "," + x + "," + y + ")";
                }
                //set parent
                tile_go.transform.SetParent(this.transform);

                //set collider
                tile_go.AddComponent<BoxCollider2D>();

                //make tile class
                Tile tiletest = new Tile(player, x, y);
            }
        }
    }
}
